module "sv-debian" {
  #source = "git::https://gitlab.com/epsipoei/mspr/terraform/vcenter/vcenter_module_vm.git"
  source = "../vcenter_module_vm"

  datacenter       = "DC1"
  cluster          = "Cluster-vc1"
  name             = "my_debian"
  cpu              = 1
  memory           = 1
  disk_size        = [8]
  template_name    = "TPL-Debian"
  datastore_names  = ["DTS-01"]
  network_names    = ["VM Network"]
  folder           = "INFRA/Linux/DMZ"
}
